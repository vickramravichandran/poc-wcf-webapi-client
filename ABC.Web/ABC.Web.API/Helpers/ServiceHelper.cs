﻿using System.ServiceModel;

namespace ABC.Web.API.Helpers
{
    public static class ServiceHelper
    {
        public static T GetService<T>()
            where T : class
        {
            var endpointName = typeof(T).FullName;
            var channelFactory = new ChannelFactory<T>(endpointName);

            return channelFactory.CreateChannel();
        }
    }
}