﻿using ABC.API.ServiceContracts.Cheshire.Benefits;
using ABC.API.ServiceContracts.Models.Benefits;
using ABC.Web.API.Helpers;
using System;
using System.Web.Http;

namespace ABC.Web.API.Controllers
{
    [RoutePrefix("api/benefits")]
    public class BenefitController : ApiController
    {
        [Route("{benefitId}")]
        public BenefitDTO GetBenefit(Guid benefitId)
        {
            var benefitService = ServiceHelper.GetService<IBenefitService>();
            var benefit = benefitService.GetBenefit(Guid.NewGuid());

            return benefit;
        }
    }
}
