﻿using ABC.API.ServiceContracts.Cheshire.Vendors;
using ABC.API.ServiceContracts.Models.Vendors;
using ABC.Web.API.Helpers;
using System;
using System.Web.Http;

namespace ABC.Web.API.Controllers
{
    [RoutePrefix("api/vendors")]
    public class VendorController : ApiController
    {
        [Route("{vendorId}")]
        public VendorDTO GetVendor(Guid vendorId)
        {
            var vendorService = ServiceHelper.GetService<IVendorService>();
            var vendor = vendorService.GetVendor(Guid.NewGuid());

            return vendor;
        }
    }
}
