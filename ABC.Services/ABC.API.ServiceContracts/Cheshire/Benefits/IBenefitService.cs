﻿using ABC.API.ServiceContracts.Models.Benefits;
using System;
using System.ServiceModel;

namespace ABC.API.ServiceContracts.Cheshire.Benefits
{
    [ServiceContract]
    public interface IBenefitService
    {
        [OperationContract]
        BenefitDTO GetBenefit(Guid benefitId);
    }
}
