﻿using ABC.API.ServiceContracts.Models.Vendors;
using System;
using System.ServiceModel;

namespace ABC.API.ServiceContracts.Cheshire.Vendors
{
    [ServiceContract]
    public interface IVendorService
    {
        [OperationContract]
        VendorDTO GetVendor(Guid vendorId);
    }
}
