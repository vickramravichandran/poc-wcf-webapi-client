﻿using System;

namespace ABC.API.ServiceContracts.Models.Vendors
{
    public class VendorDTO
    {
        public Guid VendorId { get; set; }

        public string VendorName { get; set; }
    }
}
