﻿using System;

namespace ABC.API.ServiceContracts.Models.Benefits
{
    public class BenefitDTO
    {
        public Guid BenefitId { get; set; }

        public string BenefitName { get; set; }
    }
}
