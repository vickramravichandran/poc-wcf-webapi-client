﻿using ABC.API.ServiceContracts.Cheshire.Benefits;
using ABC.API.ServiceContracts.Models.Benefits;
using System;

namespace ABC.API.Services.Cheshire.Benefits
{
    public class BenefitService : IBenefitService
    {
        public BenefitDTO GetBenefit(Guid benefitId)
        {
            return new BenefitDTO()
            {
                BenefitId = benefitId,
                BenefitName = "Auto & Home"
            };
        }
    }
}
