﻿using ABC.API.ServiceContracts.Cheshire.Vendors;
using ABC.API.ServiceContracts.Models.Vendors;
using System;

namespace ABC.API.Services.Cheshire.Vendors
{
    public class VendorService : IVendorService
    {
        public VendorDTO GetVendor(Guid vendorId)
        {
            return new VendorDTO()
            {
                VendorId = vendorId,
                VendorName = "Metlife Insurance"
            };
        }
    }
}
