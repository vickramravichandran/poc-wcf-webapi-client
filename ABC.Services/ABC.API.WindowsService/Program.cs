﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Configuration;

namespace ABC.API.WindowsService
{
    class Program
    {
        private static readonly string ABC_API_Services_ASSEMBLY = "ABC.API.Services";
        private static ServiceHost[] _serviceHosts = null;

        static void Main(string[] args)
        {
            StartService();
        }

        private static void StartService()
        {
            try
            {
                _serviceHosts = CreateServiceHosts().ToArray();
                foreach (var host in _serviceHosts)
                {
                    host.Open();
                }

                Console.WriteLine("Service is running");
            }
            catch (Exception ex)
            {
                _serviceHosts = null;
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxx");
                Console.WriteLine("Service can not be started");
                Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxx");
                Console.WriteLine(ex);
            }

            Console.Read();
        }

        private static List<ServiceHost> CreateServiceHosts()
        {
            var servicesSection = GetServicesConfigSection();
            var hosts = new List<ServiceHost>();

            foreach (ServiceElement serviceElement in servicesSection.Services)
            {
                hosts.Add(CreateServiceHost(serviceElement));
            }

            return hosts;
        }

        private static ServiceHost CreateServiceHost(ServiceElement serviceElement)
        {
            var serviceType = Type.GetType(serviceElement.Name + "," + ABC_API_Services_ASSEMBLY);
            return new ServiceHost(serviceType);
        }

        private static ServicesSection GetServicesConfigSection()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var serviceModelGroup = ServiceModelSectionGroup.GetSectionGroup(config);

            return serviceModelGroup.Services;
        }
    }
}
